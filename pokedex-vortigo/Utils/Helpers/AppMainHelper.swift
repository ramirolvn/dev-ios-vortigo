//
//  AppMainHelper.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
import UIKit

class AppMainHelper: NSObject {
	static let shared: AppMainHelper = AppMainHelper()
	
	@objc func getTopMostViewController(
		base: UIViewController? = UIApplication.shared.connectedScenes
		.filter({$0.activationState == .foregroundActive})
		.map({$0 as? UIWindowScene})
		.compactMap({$0})
		.first?.windows
		.filter({$0.isKeyWindow}).first?.rootViewController
	) -> UIViewController? {
		var baseVC: UIViewController?
		if let base = base {
			baseVC = base
		} else {
			baseVC = UIApplication.shared.connectedScenes
				.filter({$0.activationState == .foregroundActive})
				.map({$0 as? UIWindowScene})
				.compactMap({$0})
				.first?.windows
				.filter({$0.isKeyWindow}).first?.rootViewController
		}
		if let nav = baseVC as? UINavigationController {
			return getTopMostViewController(base: nav.visibleViewController)
		}
		if let tab = baseVC as? UITabBarController {
			if let selected = tab.selectedViewController {
				return getTopMostViewController(base: selected)
			}
		}
		if let presented = baseVC?.presentedViewController {
			return getTopMostViewController(base: presented)
		}
		return baseVC
	}
	
	@objc func isSimulator() -> Bool {
		#if targetEnvironment(simulator)
		return true
		#else
		return false
		#endif
	}
	
	func clearCache(){
		let cacheURL =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
		let fileManager = FileManager.default
		do {
			// Get the directory contents urls (including subfolders urls)
			let directoryContents = try FileManager.default
				.contentsOfDirectory( at: cacheURL, includingPropertiesForKeys: nil, options: [])
			for file in directoryContents {
				do {
					try fileManager.removeItem(at: file)
				}
				catch let error as NSError {
					debugPrint(error)
				}
				
			}
		} catch let error as NSError {
			print(error.localizedDescription)
		}
	}
}
