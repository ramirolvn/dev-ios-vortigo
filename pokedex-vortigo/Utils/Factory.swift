//
//  Factory.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation

protocol FactoryProtocol {
    //If necessary add other service like storage for example or other
    var provider: NetworkManagerProtocol { get set }
	var keychain: KeychainProtocol { get set }
}

class FactoryBuilder {
    typealias FactoryBuilderClosure = (FactoryBuilder) -> ()
    var provider: NetworkManagerProtocol!
	var keychain: KeychainProtocol!
    
    init(factoryBuilder: FactoryBuilderClosure) {
        factoryBuilder(self)
    }
}

class Factory: FactoryProtocol {
    var provider: NetworkManagerProtocol
	var keychain: KeychainProtocol
    
    init(factoryBuilder: FactoryBuilder) {
        provider = factoryBuilder.provider
		keychain = factoryBuilder.keychain
    }
}
