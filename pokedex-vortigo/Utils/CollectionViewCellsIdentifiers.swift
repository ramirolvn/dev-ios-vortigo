//
//  CollectionViewCellsIdentifiers.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation

enum CollectionViewCellsIdentifiers {
	static let pokemonTypeCollection = "PokemonTypeCollectionViewCell"
}
