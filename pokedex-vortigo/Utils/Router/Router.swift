//
//  Router.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 20/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit
import RxSwift

class Router: RouterProtocol {
    var window: UIWindow?
    
    var viewModelFactory: ViewModelFactoryProtocol
    
    init(vmFactory: ViewModelFactoryProtocol) {
        viewModelFactory = vmFactory
    }
    
    func nav(_ vc: UIViewController) -> UINavigationController {
        let nav = UINavigationController(rootViewController: vc)
        return nav
    }
    
    func wrappedViewController(for screen: ApplicationScreen, data: Any?) -> UIViewController {
        let vc = screen.viewController()
        let vm = viewModelFactory.viewModel(for: screen, data: data)
        vc.baseViewModel = vm
        var wrapped: UIViewController = vc
        
        switch screen {
		case .splash,.login:
            //needs navigation?
            wrapped = nav(vc)
        default:
            //do not need navigation
            wrapped = vc
        }
        
        return wrapped
    }
    
    func pushToNavigate(from viewController: UIViewController, to screen: ApplicationScreen, data: Any?) {
        if let navigation = viewController.navigationController {
            let vc = wrappedViewController(for: screen, data: data)
            navigation.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - Defaults
    func route(from viewController: UIViewController, to screen: ApplicationScreen, data: Any?, action: RouteAction = .push, completion: (() -> Void)?) {
        switch action {
        case .push:
            pushToNavigate(from: viewController, to: screen, data: data)
            guard let end = completion else { return }
            end()
        case .pop:
            guard let navigation = viewController.navigationController else { return }
            navigation.popViewController(animated: true)
            guard let end = completion else { return }
            end()
        case .dismiss:
            viewController.dismiss(animated: true) {
                guard let end = completion else { return }
                end()
            }
        case .present:
            let vc = wrappedViewController(for: screen, data: data)
            viewController.present(vc, animated: true) {
                guard let end = completion else { return }
                end()
            }
        case .modal:
            let vc = wrappedViewController(for: screen, data: data)
            if let customVc = viewController as? CustomPresentationProtocol {
                customVc.slideInTransitioningDelegate.direction = .bottom
                vc.transitioningDelegate = customVc.slideInTransitioningDelegate
				vc.modalPresentationStyle = .formSheet
            }
            viewController.present(vc, animated: true) {
                guard let end = completion else { return }
                end()
            }
        case .popover:
            let vc = wrappedViewController(for: screen, data: data)
            let (dataView, _, _) = data as! (UIView, Any?, Any?)
            vc.modalPresentationStyle = .popover
            vc.popoverPresentationController?.delegate = viewController as? UIPopoverPresentationControllerDelegate
            vc.popoverPresentationController?.sourceView = dataView
            vc.popoverPresentationController?.sourceRect = dataView.bounds
            vc.preferredContentSize = CGSize(width: 250.0, height: 88.0)
            viewController.present(vc, animated: true, completion: nil)
        }
    }
    
    func route(from viewController: UIViewController, to screen: ApplicationScreen) {
        self.route(from: viewController, to: screen, data: nil, action: .push, completion: nil)
    }
    
    func route(from viewController: UIViewController, to screen: ApplicationScreen, data: Any? = nil) {
        self.route(from: viewController, to: screen, data: data, action: .push, completion: nil)
    }
    
    func route(from viewController: UIViewController, to screen: ApplicationScreen, action: RouteAction = .push) {
        self.route(from: viewController, to: screen, data: nil, action: action, completion: nil)
    }
    
    func route(from viewController: UIViewController, to screen: ApplicationScreen, data: Any?, action: RouteAction) {
        self.route(from: viewController, to: screen, data: data, action: action, completion: nil)
    }
    
    // MARK: - Modal
    private func modalRoute(from viewController: UIViewController, to screen: ApplicationScreen, data: Any?, action: RouteAction = .modal, height: CGFloat?, modalMode: ModalMode) {
        switch modalMode {
        case .from:
            viewController.dismiss(animated: true) {
				guard let root = AppMainHelper.shared.getTopMostViewController(), height == nil else { return }
                self.route(from: root, to: screen, data: data, action: action, completion: nil)
            }
        case .to:
            guard var customVC = viewController as? CustomPresentationProtocol,
                let height = height else { return }
            customVC.slideInTransitioningDelegate = CustomModalPresentationManager(nextViewHeight: height)
            self.route(from: viewController, to: screen, data: data, action: .modal, completion: nil)
        case .between:
            viewController.dismiss(animated: true) {
				guard let root = AppMainHelper.shared.getTopMostViewController(),
                    var customRoot = root as? CustomPresentationProtocol,
                    let height = height else { return }
                customRoot.slideInTransitioningDelegate = CustomModalPresentationManager(nextViewHeight: height)
                self.route(from: root, to: screen, data: data, action: .modal, completion: nil)
            }
        }
    }
    
    func route(from viewController: UIViewController, to screen: ApplicationScreen, height: CGFloat, modalMode: ModalMode) {
        self.modalRoute(from: viewController, to: screen, data: nil, height: height, modalMode: modalMode)
    }
    
    func route(from viewController: UIViewController, to screen: ApplicationScreen, data: Any?, height: CGFloat, modalMode: ModalMode) {
        self.modalRoute(from: viewController, to: screen, data: data, height: height, modalMode: modalMode)
    }
    
    func route(from viewController: UIViewController, to screen: ApplicationScreen, action: RouteAction, modalMode: ModalMode) {
        self.modalRoute(from: viewController, to: screen, data: nil, action: action, height: nil, modalMode: modalMode)
    }
    
    func route(from viewController: UIViewController, to screen: ApplicationScreen, data: Any?, action: RouteAction, modalMode: ModalMode) {
        self.modalRoute(from: viewController, to: screen, data: data, action: action, height: nil, modalMode: modalMode)
    }
    
    //MARK: - Root
    func changeRoot(to screen: ApplicationScreen) {
        self.window?.rootViewController = wrappedViewController(for: screen, data: nil)
    }
    
    //MARK: - Unwind
    func unwind(from viewController: UIViewController, to screen: UnwindScreen) {
        viewController.performSegue(withIdentifier: screen.unwindItem, sender: viewController)
    }
    
}
