//
//  Protocols.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 20/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit

protocol BaseViewModelProtocol { }

class BaseViewModel: BaseViewModelProtocol { }

protocol BaseController: UIPopoverPresentationControllerDelegate {
    var baseViewModel: BaseViewModelProtocol! { get set }
}

protocol StoryboardItem {
    static func containerStoryboard() -> ApplicationStoryboard
    static func nameInStoryboard() -> String
}

extension StoryboardItem {
    static func nameInStoryboard() -> String {
        return String(describing: self)
    }
}

typealias BaseVController = UIViewController & BaseController

protocol RouterProtocol {
    var window: UIWindow? { get set }
    var viewModelFactory: ViewModelFactoryProtocol { get set }
    func route(from viewController: UIViewController, to screen: ApplicationScreen)
    func route(from viewController: UIViewController, to screen: ApplicationScreen, data: Any?)
    func route(from viewController: UIViewController, to screen: ApplicationScreen, data: Any?, action: RouteAction)
    func route(from viewController: UIViewController, to screen: ApplicationScreen, action: RouteAction)
    func route(from viewController: UIViewController, to screen: ApplicationScreen, data: Any?, action: RouteAction, completion: (() -> Void)?)
    func route(from viewController: UIViewController, to screen: ApplicationScreen, height: CGFloat, modalMode: ModalMode)
    func route(from viewController: UIViewController, to screen: ApplicationScreen, data: Any?, height: CGFloat, modalMode: ModalMode)
    func route(from viewController: UIViewController, to screen: ApplicationScreen, action: RouteAction, modalMode: ModalMode)
    func route(from viewController: UIViewController, to screen: ApplicationScreen, data: Any?, action: RouteAction, modalMode: ModalMode)
    func changeRoot(to screen: ApplicationScreen)
    func unwind(from viewController: UIViewController, to screen: UnwindScreen)
}
