//
//  ManagerCenter.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 20/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation

typealias ManagerConfiguration = (
    factory: FactoryProtocol,
    router: RouterProtocol,
    viewModelFactory: ViewModelFactoryProtocol
)

class ManagerCenter {
    
    static let shared: ManagerCenter = ManagerCenter()
    
    var router: RouterProtocol
    private(set) var factory: FactoryProtocol
    private(set) var viewModelFactory: ViewModelFactoryProtocol
    private static var configuration: ManagerConfiguration?
    
    //used on device change only
    var tempPassword: String?
    
    class func configure(config: ManagerConfiguration) {
        ManagerCenter.configuration = config
    }
    
    private init() {
        let configuration = ManagerCenter.configuration
        guard let config = configuration else {
            fatalError("Error - You must call setup before accessing ManagerCenter.shared")
        }
        router = config.router
        factory = config.factory
        viewModelFactory = config.viewModelFactory
    }
}
