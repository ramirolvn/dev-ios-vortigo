//
//  UIImage+URL+Extension.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit
extension UIImageView {
	public func imageFromURL(urlString: String) {
		let activityIndicator = UIActivityIndicatorView(style: .medium)
		activityIndicator.color = Colors.pinkRed
		activityIndicator.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
		activityIndicator.startAnimating()
		if self.image == nil{
			self.addSubview(activityIndicator)
		}
		
		URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
			
			if error != nil {
				print(error ?? "No Error")
				return
			}
			DispatchQueue.main.async(execute: { () -> Void in
				let image = UIImage(data: data!)
				activityIndicator.removeFromSuperview()
				self.image = image
			})
			
		}).resume()
	}
}
