//
//  UIViewController+Loaders+Extension.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
import UIKit
fileprivate let loaderTag = 9342410334

extension UIViewController {
	
	// MARK: - Activity Indicator
	func start() {
		
		DispatchQueue.main.async { [weak self] in
			guard let weakself = self else { return }
			let loader = DefaultCustomLoader(frame: UIScreen.main.bounds)
			
			loader.tag = loaderTag
			
			let screen = UIScreen.main.bounds
			loader.center = CGPoint(x: screen.midX, y: screen.midY)
			loader.frame = screen
			
			guard let navigation = weakself.navigationController else {
				weakself.view.addSubview(loader)
				return
			}
			navigation.view.addSubview(loader)
		}
	}
	
	func stop() {
		
		DispatchQueue.main.async { [weak self] in
			guard let weakself = self else { return }
			let root = weakself.navigationController ?? weakself
			if let view = root.view.viewWithTag(loaderTag) {
				view.removeFromSuperview()
			}
		}
	}
}
