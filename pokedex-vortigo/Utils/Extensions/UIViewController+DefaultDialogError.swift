//
//  UIViewController+DefaultDialogError.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit

extension UIViewController {
	
	// MARK: - Activity Indicator
	func showDefaultServerAlertError(actionButtonClosure: @escaping () -> Void) {
		let alertController = UIAlertController(title: "Atenção", message: "Algo inesperado aconteceu. Tente novamente mais tarde", preferredStyle: .alert)
		let tryAgain = UIAlertAction(title: "Tentar Novamente", style: .default) { action in
			actionButtonClosure()
		}
		let tryLater = UIAlertAction(title: "Cancelar", style: .default) { action in
			//			print("Cancelar")
		}
		alertController.addAction(tryAgain)
		alertController.addAction(tryLater)
		self.present(alertController, animated: true, completion: nil)
	}
}
