//
//  DefaultKeychain.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
import Security

protocol KeychainProtocol: class {
	func saveUser(_ user: User) -> OSStatus
	func loadUser() -> User?
	func deleteUser()  -> OSStatus
}

class KeyChain: KeychainProtocol {
	
	func saveUser(_ user: User) -> OSStatus {
		let jsonEnconder = JSONEncoder()
		guard let userEncoded = try? jsonEnconder.encode(user) else{
			return .max
		}
		let query = [
			kSecClass as String       : kSecClassGenericPassword as String,
			kSecAttrAccount as String : "userPreference",
			kSecValueData as String   : userEncoded ] as [String : Any]
		
		SecItemDelete(query as CFDictionary)
		let status = SecItemAdd(query as CFDictionary, nil)
		return status
	}
	
	func loadUser() -> User? {
		let decoder = JSONDecoder()
		let query = [
			kSecClass as String       : kSecClassGenericPassword,
			kSecAttrAccount as String : "userPreference",
			kSecReturnData as String  : kCFBooleanTrue!,
			kSecMatchLimit as String  : kSecMatchLimitOne ] as [String : Any]
		
		var dataTypeRef: AnyObject? = nil
		
		let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)
		
		guard status == noErr, let userData = dataTypeRef as? Data, let loadedUser = try? decoder.decode(User.self, from: userData) else {
			return nil
		}
		return loadedUser
	}
	
	func deleteUser()  -> OSStatus {
		let query = [
			kSecClass as String       : kSecClassGenericPassword as String,
			kSecAttrAccount as String : "userPreference"] as [String : Any]
		
		SecItemDelete(query as CFDictionary)
		let status = SecItemDelete(query as CFDictionary)
		return status
	}
}

