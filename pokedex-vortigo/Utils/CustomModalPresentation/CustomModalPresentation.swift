//
//  CustomModalPresentation.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
import UIKit

enum PresentationDirection {
	case left
	case right
	case top
	case bottom
}

protocol CustomPresentationProtocol {
	var slideInTransitioningDelegate: CustomModalPresentationManager { get set }
}

class CustomModalPresentationManager: NSObject {
	var direction: PresentationDirection = .bottom
	var height: CGFloat
	
	init(nextViewHeight: CGFloat = 0.0) {
		self.height = nextViewHeight
	}
}

extension CustomModalPresentationManager: UIViewControllerTransitioningDelegate {
	func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
		let presentationController = CustomModalPresentationController(presentedViewController: presented, presenting: presenting, direction: direction, height: height)
		return presentationController
	}
	
	func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		return CustomModalPresationAnimator(direction: direction, isPresentation: true)
	}
	
	func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		return CustomModalPresationAnimator(direction: direction, isPresentation: false)
	}
}
