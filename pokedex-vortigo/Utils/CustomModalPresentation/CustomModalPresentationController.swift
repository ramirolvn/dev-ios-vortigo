//
//  CustomModalPresentationController.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
import UIKit

class CustomModalPresentationController: UIPresentationController {
	fileprivate var dimmingView: UIView!
	private var direction: PresentationDirection
	private var height: CGFloat
	
	init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?, direction: PresentationDirection, height: CGFloat) {
		self.direction = direction
		self.height = height
		super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
		setupDimmingView()
	}
	
	override func presentationTransitionWillBegin() {
		containerView?.insertSubview(dimmingView, at: 0)
		
		NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|[dimmingView]|", options: [], metrics: nil, views: ["dimmingView": dimmingView!]))
		NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|[dimmingView]|", options: [], metrics: nil, views: ["dimmingView": dimmingView!]))
		
		guard let coordinator = presentedViewController.transitionCoordinator else {
			dimmingView.alpha = 1.0
			return
		}
		
		coordinator.animate(alongsideTransition: { (_) in
			self.dimmingView.alpha = 1.0
		})
	}
	
	override func dismissalTransitionWillBegin() {
		guard let coordinator = presentedViewController.transitionCoordinator else {
			dimmingView.alpha = 0.0
			return
		}
		
		coordinator.animate(alongsideTransition: { (_) in
			self.dimmingView.alpha = 0.0
		})
	}
	
	override func containerViewDidLayoutSubviews() {
		presentedView?.frame = frameOfPresentedViewInContainerView
	}
	
	override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
		switch direction {
		case .left, .right:
			return CGSize(width: parentSize.width * (2.0/3.0), height: parentSize.height)
		case .bottom, .top:
			guard let window = UIApplication.shared.connectedScenes
				.filter({$0.activationState == .foregroundActive})
				.map({$0 as? UIWindowScene})
				.compactMap({$0})
				.first?.windows
				.filter({$0.isKeyWindow}).first else { return CGSize.zero }
			let topPadding = window.safeAreaInsets.top
			return CGSize(width: parentSize.width, height: (topPadding + height))
		}
	}
	
	override var frameOfPresentedViewInContainerView: CGRect {
		var frame: CGRect = .zero
		frame.size = size(forChildContentContainer: presentedViewController, withParentContainerSize: containerView!.bounds.size)
		
		switch direction {
		case .bottom:
			frame.origin.y = containerView!.bounds.size.height - (UIApplication.shared.windows.first?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0 + height)
			
		default:
			frame.origin = .zero
		}
		return frame
	}
}

private extension CustomModalPresentationController {
	func setupDimmingView() {
		dimmingView = UIView()
		dimmingView.translatesAutoresizingMaskIntoConstraints = false
		dimmingView.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
		dimmingView.alpha = 0.0
		
		let recognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(recongnizer:)))
		dimmingView.addGestureRecognizer(recognizer)
	}
	
	@objc dynamic func handleTap(recongnizer: UITapGestureRecognizer) {
		presentingViewController.dismiss(animated: true)
	}
}
