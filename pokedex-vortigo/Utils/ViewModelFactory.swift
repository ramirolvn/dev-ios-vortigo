//
//  ViewModelFactory.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 20/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol ViewModelFactoryProtocol {
	func viewModel(for screen: ApplicationScreen) -> BaseViewModelProtocol
	func viewModel(for screen: ApplicationScreen, data: Any?) -> BaseViewModelProtocol
	init(factory: FactoryProtocol)
}

class ViewModelFactory: ViewModelFactoryProtocol {
	
	private var factory: FactoryProtocol
	
	required init(factory: FactoryProtocol) {
		self.factory = factory
	}
	
	func viewModel(for screen: ApplicationScreen) -> BaseViewModelProtocol {
		return viewModel(for: screen, data: nil)
	}
	
	func viewModel(for screen: ApplicationScreen, data: Any?) -> BaseViewModelProtocol{
		var viewModel: BaseViewModelProtocol = BaseViewModel()
		switch screen {
		case .splash:
			let keychain = ManagerCenter.shared.factory.keychain
			viewModel = SplashViewModel(keychainProtocol: keychain)
		case .login:
			viewModel = LoginViewModel()
		case .registerUserName:
			viewModel = RegisterUserNameViewModel()
		case .choosePokemonType:
			let provider = ManagerCenter.shared.factory.provider
			let keychain = ManagerCenter.shared.factory.keychain
			let userName: String = data as! String
			viewModel = ChoosePokemonTypeViewModel(provider, userName: userName, keychainProtocol: keychain)
		case .pokemonTypeList:
			let provider = ManagerCenter.shared.factory.provider
			let choosePokemonTypeControllerDelegate = data as! ChoosePokemonTypeViewControllerProtocol
			viewModel = PokemonTypeViewModel(provider, choosePokemonTypeControllerDelegate: choosePokemonTypeControllerDelegate)
		case .home:
			let provider = ManagerCenter.shared.factory.provider
			let keychain = ManagerCenter.shared.factory.keychain
			viewModel = HomeViewModel(provider, keychain: keychain)
		}
		return viewModel
	}
}
