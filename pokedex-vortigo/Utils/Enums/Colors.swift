//
//  Colors.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit

enum Colors {
    static let pinkRed = #colorLiteral(red: 0.8705882353, green: 0.231372549, blue: 0.462745098, alpha: 1)
}
