//
//  RouterEnumeration.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
import UIKit

enum RouteAction {
	case push
	case pop
	case present
	case dismiss
	case modal
	case popover
}

enum ModalMode {
	case from
	case to
	case between
}

enum ApplicationScreen {
	case splash
	case login
	case registerUserName
	case choosePokemonType
	case pokemonTypeList
	case home
	
	var storyboardItem: StoryboardItem.Type {
		switch self {
		case .splash:
			return SplashController.self
		case .login:
			return LoginController.self
		case .registerUserName:
			return RegisterUserNameViewController.self
		case .choosePokemonType:
			return ChoosePokemonTypeViewController.self
		case .pokemonTypeList:
			return PokemonTypeListController.self
		case .home:
			return HomeController.self
		}
	}
	
	func viewController() -> BaseVController {
		return self.storyboardItem.containerStoryboard().storyboard
			.instantiateViewController(withIdentifier: self.storyboardItem.nameInStoryboard()) as! BaseVController
	}
}

enum ApplicationStoryboard: String {
	case main = "Main"
	case splash = "Splash"
	var storyboard: UIStoryboard {
		return UIStoryboard(name: self.rawValue, bundle: nil)
	}
}

enum UnwindScreen {
	case toHome
	
	var unwindItem: String {
		switch self {
		case .toHome:
			return "unwindToHome"
		}
	}
}

