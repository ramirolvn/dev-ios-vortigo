//
//  AppInitialConfigs.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 20/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
import UIKit



class AppInitialConfig: NSObject {
	
	@objc static let shared: AppInitialConfig = AppInitialConfig()
	public var ENVIRONMENT: Environment?
	
	private override init() {
	}
	
	@objc func prepare(window: UIWindow) {
		configEnvironment()
		config()
		ManagerCenter.shared.router.window = window
		ManagerCenter.shared.router.changeRoot(to: .splash)
	}
	
	// MARK: - configs
	func config() {
		
		let factoryBuilder = FactoryBuilder { (fb) in
			fb.provider = NetworkManager()
			fb.keychain = KeyChain()
		}
		
		let factory = Factory(factoryBuilder: factoryBuilder)
		let vmFactory = ViewModelFactory(factory: factory)
		let router: Router = Router(vmFactory: vmFactory)
		ManagerCenter.configure(config:
			ManagerConfiguration(factory: factory,
								 router: router,
								 viewModelFactory: vmFactory)
		)
	}
	
	func configEnvironment() {
		#if PRODUCTION
		self.ENVIRONMENT = .prod
		#else
		self.ENVIRONMENT = .dev
		#endif
	}
}
