//
//  PokemonCell.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit

class PokemonCell: UITableViewCell {
	@IBOutlet weak var pImg: UIImageView!
	@IBOutlet weak var pName: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	func config(pokemon: Pokemon){
		self.pImg.imageFromURL(urlString: pokemon.thumbnailImage)
		self.pName.text = pokemon.name
	}
	
}
