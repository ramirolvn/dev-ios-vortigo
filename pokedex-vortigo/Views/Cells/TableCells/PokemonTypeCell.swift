//
//  PokemonTypeCell.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit

class PokemonTypeCell: UITableViewCell {
	
	
	@IBOutlet weak var pTypeImg: UIImageView!
	@IBOutlet weak var pTypeName: UILabel!
	@IBOutlet weak var isFavoriteButton: UIButton!
	
	var clickFavoriteButton : ((PokemonType?)->())?
	var pokemonType: PokemonType?
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	func config(pokemonType: PokemonType, isTheFavorite: Bool){
		self.pokemonType = pokemonType
		self.pTypeImg.imageFromURL(urlString: pokemonType.thumbnailImage)
		self.pTypeName.text = pokemonType.name.capitalized
		let newBtnImg = isTheFavorite ?  #imageLiteral(resourceName: "radio-on") : #imageLiteral(resourceName: "radio-off")
		self.isFavoriteButton.setImage(newBtnImg, for: .normal)
	}
	
	@IBAction func clickBtn(_ sender: UIButton) {
		if let clickFavoriteButton = clickFavoriteButton{
			clickFavoriteButton(pokemonType)
		}
	}
}
