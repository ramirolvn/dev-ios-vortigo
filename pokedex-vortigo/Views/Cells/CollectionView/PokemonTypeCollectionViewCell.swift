//
//  PokemonTypeCellViewCell.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit

class PokemonTypeCollectionViewCell: UICollectionViewCell {
	@IBOutlet weak var pokemonTypeImg: UIImageView!
	@IBOutlet weak var pokemonTypeName: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	func config(pokemonType: PokemonType, isTheSelected: Bool){
		self.pokemonTypeImg.imageFromURL(urlString: pokemonType.thumbnailImage)
		self.pokemonTypeName.text = pokemonType.name.capitalized
		self.pokemonTypeName.textColor = isTheSelected ? Colors.pinkRed : .black
	}
	
}
