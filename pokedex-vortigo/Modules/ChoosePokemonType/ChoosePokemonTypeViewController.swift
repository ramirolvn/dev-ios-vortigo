//
//  ChoosePokemonTypeViewController.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit
import RxSwift

protocol ChoosePokemonTypeViewControllerProtocol: class {
	func selectedFavorite(pokemonType: PokemonType?)
}

class ChoosePokemonTypeViewController: UIViewController, BaseController, CustomPresentationProtocol, ChoosePokemonTypeViewControllerProtocol {
	
	var slideInTransitioningDelegate: CustomModalPresentationManager = CustomModalPresentationManager()
	var baseViewModel: BaseViewModelProtocol! {
		didSet {
			viewModel = (baseViewModel as! ChoosePokemonTypeViewModelProtocol)
		}
	}
	
	var viewModel: ChoosePokemonTypeViewModelProtocol!
	
	@IBOutlet weak var titleLb: UILabel!
	@IBOutlet weak var continueBtn: UIButton!
	@IBOutlet weak var chooseTypeTF: UnderlineDropdownTF!
	let disposeBag = DisposeBag()
	
	override var prefersStatusBarHidden: Bool {
		return true
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		configView()
	}
	
	private func configView(){
		self.titleLb.text = viewModel.titleLabelText
		self.continueBtn.isEnabled = false
		self.chooseTypeTF.delegate = self
		configObservables()
		self.chooseTypeTF.touchInsideImg = { [weak self] in
			guard let strongSelf = self else { return }
			ManagerCenter.shared.router.route(from: strongSelf,
											  to: .pokemonTypeList,
											  data: strongSelf,
											  height: strongSelf.view.frame.height*0.65,
											  modalMode: .to)
		}
	}
	
	private func configObservables(){
		viewModel.saveSucess.subscribe { [weak self] (event) in
			guard let weakself = self, let isSaved = event.event.element else { return }
			weakself.stop()
			isSaved ? ManagerCenter.shared.router.changeRoot(to: .home) : weakself.showDefaultServerAlertError {[weak self] in
				guard let strongSelf = self else { return }
				strongSelf.start()
				strongSelf.viewModel.saveUser()
			}
		}.disposed(by: disposeBag)
	}
	
	func selectedFavorite(pokemonType: PokemonType?) {
		guard let pokemonType = pokemonType else { return }
		self.chooseTypeTF.text = pokemonType.name.capitalized
		self.viewModel.favoriteUser = pokemonType
		self.continueBtn.isEnabled = true
	}
	
	@IBAction func backAction(_ sender: UIButton) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func saveUser(_ sender: UIButton) {
		guard chooseTypeTF.text != nil else {
			return
		}
		self.start()
		viewModel.saveUser()
	}
	
	
}
extension ChoosePokemonTypeViewController: StoryboardItem {
	static func containerStoryboard() -> ApplicationStoryboard {
		return .main
	}
}
extension ChoosePokemonTypeViewController: UITextFieldDelegate{
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		return false
	}
}
