//
//  ChoosePokemonTypeViewModel.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
import RxCocoa

protocol ChoosePokemonTypeViewModelProtocol: BaseViewModelProtocol {
	var titleLabelText:String { get }
	var favoriteUser: PokemonType? {get set}
	var saveSucess:PublishRelay<Bool> { get }
	func saveUser()
}

class ChoosePokemonTypeViewModel: ChoosePokemonTypeViewModelProtocol {
	
	private let provider: NetworkManagerProtocol
	weak var keychainProtocol: KeychainProtocol? = nil
	let userName: String
	var favoriteUser: PokemonType? = nil
	var saveSucess:PublishRelay<Bool>  = PublishRelay()
	
	
	var titleLabelText:String {
		return "Hello, \(userName)"
	}
	
	init(_ provider: NetworkManagerProtocol, userName: String, keychainProtocol: KeychainProtocol) {
		self.provider = provider
		self.userName = userName
		self.keychainProtocol = keychainProtocol
		print("INITIALIZATION -> ChoosePokemonTypeViewModel")
	}
	
	deinit {
		print("DEINITIALIZATION -> ChoosePokemonTypeViewModel")
	}
	
	func saveUser(){
		guard let keychain = self.keychainProtocol, let favoritePokemonType = self.favoriteUser else{
			self.saveSucess.accept(false)
			return
		}
		let newUser = User(userName: self.userName, pokemonTypeFavorite: favoritePokemonType)
		self.saveSucess.accept(keychain.saveUser(newUser) == .zero)
	}
}
