//
//  RegisterUserNameViewController.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit

class RegisterUserNameViewController: UIViewController, BaseController {
	
	var baseViewModel: BaseViewModelProtocol! {
		didSet {
			viewModel = (baseViewModel as! RegisterUserNameViewModelProtocol)
		}
	}
	
	var viewModel: RegisterUserNameViewModelProtocol!
	
	override var prefersStatusBarHidden: Bool {
		return true
	}
	
	@IBOutlet weak var userNameTF: DefaultUnderlineWhiteTF!
	
	@IBOutlet weak var continueBtn: UIButton!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		configView()
	}
	
	private func configView(){
		self.userNameTF.delegate = self
		self.continueBtn.isEnabled = false
	}
	
	@IBAction func continueRegisterAction(_ sender: UIButton) {
		guard let userName: String = self.userNameTF.text else {
			fatalError("Error in outlet")
		}
		ManagerCenter.shared.router.route(from: self, to: .choosePokemonType, data: userName)
	}
	
	@IBAction func backAction(_ sender: UIButton) {
		self.navigationController?.popViewController(animated: true)
	}
	
	
}
extension RegisterUserNameViewController: StoryboardItem {
	static func containerStoryboard() -> ApplicationStoryboard {
		return .main
	}
}

extension RegisterUserNameViewController: UITextFieldDelegate{
	func textFieldDidEndEditing(_ textField: UITextField) {
		guard let userName = textField.text else {
			fatalError("Error in outlet")
		}
		self.continueBtn.isEnabled = viewModel.isValidUsername(userName: userName)
	}
}
