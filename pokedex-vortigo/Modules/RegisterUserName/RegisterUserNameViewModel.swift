//
//  RegisterUserNameViewModel.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation

protocol RegisterUserNameViewModelProtocol: BaseViewModelProtocol {
	func isValidUsername(userName: String)-> Bool
}

class RegisterUserNameViewModel: RegisterUserNameViewModelProtocol {
	
	init() {
		print("INITIALIZATION -> RegisterUserNameViewModel")
	}
	
	deinit {
		print("DEINITIALIZATION -> RegisterUserNameViewModel")
	}
	
	func isValidUsername(userName: String)-> Bool{
		return userName.count > 0
	}
}
