//
//  LoginController.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit

class LoginController: UIViewController, BaseController {
	
	var baseViewModel: BaseViewModelProtocol! {
		didSet {
			viewModel = (baseViewModel as! LoginViewModelProtocol)
		}
	}
	
	var viewModel: LoginViewModelProtocol!
	override var prefersStatusBarHidden: Bool {
		return true
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}
	
	override func viewWillAppear(_ animated: Bool) {
		self.navigationController?.setNavigationBarHidden(true, animated: false)
	}
	
	@IBAction func registerAction(_ sender: RedMainButton) {
		ManagerCenter.shared.router.route(from: self, to: .registerUserName)
	}
}
extension LoginController: StoryboardItem {
	static func containerStoryboard() -> ApplicationStoryboard {
		return .main
	}
}
