//
//  PokemonTypeListController.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit
import RxSwift

class PokemonTypeListController: UIViewController, BaseController {
	
	var baseViewModel: BaseViewModelProtocol! {
		didSet {
			viewModel = (baseViewModel as! PokemonTypeViewModelProtocol)
		}
	}
	
	var viewModel: PokemonTypeViewModelProtocol!
	
	@IBOutlet weak var typesList: UITableView!
	@IBOutlet weak var confirmBtn: RedMainButton!
	
	let disposeBag =  DisposeBag()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		configView()
	}
	
	private func configView(){
		self.confirmBtn.disable()
		makeRequests()
		configureObservables()
		self.typesList.dataSource = self
		typesList.register(UINib(nibName: TableViewCellIdentifiers.pokemonTypeCell, bundle: nil),
						   forCellReuseIdentifier: TableViewCellIdentifiers.pokemonTypeCell)
	}
	
	private func configureObservables(){
		viewModel.pokemonTypesResults.subscribe { [weak self] (pokemonTypesResults) in
			guard let weakself = self, let _ = pokemonTypesResults.element else { return }
			weakself.typesList.reloadData()
			weakself.stop()
		}.disposed(by: disposeBag)
		
		viewModel.requestFailure.subscribe { [weak self] (event) in
			guard let weakself = self else { return }
			weakself.stop()
			weakself.showDefaultServerAlertError {[weak self] in
				guard let strongSelf = self else { return }
				strongSelf.start()
				strongSelf.viewModel.getAllPokemonTypes(disposeBag: strongSelf.disposeBag)
			}
		}.disposed(by: disposeBag)
	}
	
	private func makeRequests(){
		self.start()
		self.viewModel.getAllPokemonTypes(disposeBag: self.disposeBag)
	}
	
	
	@IBAction func closeAction(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func confirmBtn(_ sender: UIButton) {
		self.dismiss(animated: true, completion: {[weak self] in
			guard let strongSelf = self else { return }
			strongSelf.viewModel.choosePokemonTypeControllerDelegate?.selectedFavorite(pokemonType: strongSelf.viewModel.favoriteType)
		})
	}
}
extension PokemonTypeListController: StoryboardItem {
	static func containerStoryboard() -> ApplicationStoryboard {
		return .main
	}
}
extension PokemonTypeListController: UITableViewDataSource{
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return viewModel.pokemonTypesResults.value?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let typeCell = self.typesList.dequeueReusableCell(withIdentifier: TableViewCellIdentifiers.pokemonTypeCell) as? PokemonTypeCell, let pokemonType = viewModel.getPokemonType(at: indexPath.item) else {
			fatalError("Error in mount view")
		}
		
		typeCell.config(pokemonType: pokemonType, isTheFavorite: viewModel.isTheFavoriteChosen(pokemonType: pokemonType))
		typeCell.clickFavoriteButton = {[weak self] (porkemonSelected) in
			guard let strongSelf = self, let porkemonSelected = porkemonSelected else{
				return
			}
			strongSelf.viewModel.favoriteType = porkemonSelected
			strongSelf.confirmBtn.enable()
			strongSelf.typesList.reloadData()
		}
		
		return typeCell
		
	}
	
}

