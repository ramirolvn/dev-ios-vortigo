//
//  PokemonTypeViewModel.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol PokemonTypeViewModelProtocol: BaseViewModelProtocol {
	var pokemonTypesResults: BehaviorRelay<[PokemonType]?> { get }
	var favoriteType: PokemonType? { get set }
	var requestFailure: PublishRelay<Bool> { get }
	var choosePokemonTypeControllerDelegate: ChoosePokemonTypeViewControllerProtocol? { get }
	func getPokemonType(at index: Int) -> PokemonType?
	func getAllPokemonTypes(disposeBag: DisposeBag)
	func isTheFavoriteChosen(pokemonType: PokemonType) -> Bool
}

class PokemonTypeViewModel: PokemonTypeViewModelProtocol {
	
	private let provider: NetworkManagerProtocol
	var pokemonTypesResults: BehaviorRelay<[PokemonType]?> = BehaviorRelay(value: nil)
	var favoriteType: PokemonType? = nil
	var requestFailure: PublishRelay<Bool> = PublishRelay()
	weak var choosePokemonTypeControllerDelegate: ChoosePokemonTypeViewControllerProtocol? = nil
	
	init(_ provider: NetworkManagerProtocol, choosePokemonTypeControllerDelegate: ChoosePokemonTypeViewControllerProtocol?) {
		self.provider = provider
		self.choosePokemonTypeControllerDelegate = choosePokemonTypeControllerDelegate
		print("INITIALIZATION -> PokemonTypeViewModel")
	}
	
	deinit {
		print("DEINITIALIZATION -> PokemonTypeViewModel")
	}
	
	
	func getAllPokemonTypes(disposeBag: DisposeBag){
		provider.getAllPokemonTypes(disposeBag: disposeBag, completion: {(pokemonsTypes,result,error) in
			switch result{
			case .success:
				self.pokemonTypesResults.accept(pokemonsTypes?.results)
			case .failure:
				self.requestFailure.accept(true)
			}
		})
	}
	
	func getPokemonType(at index: Int) -> PokemonType?{
		guard let pokemonResults = self.pokemonTypesResults.value, pokemonResults.count > index else {
			return nil
		}
		return pokemonResults[index]
	}
	
	func isTheFavoriteChosen(pokemonType: PokemonType) -> Bool{
		return pokemonType.name == favoriteType?.name && pokemonType.thumbnailImage == favoriteType?.thumbnailImage
	}
}
