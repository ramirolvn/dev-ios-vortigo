//
//  SplashViewModel.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation

protocol SplashViewModelProtocol: BaseViewModelProtocol {
	func getSavedUser() -> User?
}

class SplashViewModel: SplashViewModelProtocol {
	
	weak var keychainProtocol: KeychainProtocol? = nil
	
	init(keychainProtocol: KeychainProtocol) {
		self.keychainProtocol = keychainProtocol
		print("INITIALIZATION -> SplashViewModel")
	}
	
	deinit {
		print("DEINITIALIZATION -> SplashViewModel")
	}
	
	func getSavedUser() -> User?{
		guard let keychain = self.keychainProtocol else{
			return nil
		}
		return keychain.loadUser()
	}
}
