//
//  SplashController.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit

class SplashController: UIViewController, BaseController {
	
	var baseViewModel: BaseViewModelProtocol! {
		didSet {
			viewModel = (baseViewModel as! SplashViewModelProtocol)
		}
	}
	
	var viewModel: SplashViewModelProtocol!
	
	override var prefersStatusBarHidden: Bool {
		return true
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		guard (self.viewModel.getSavedUser()) != nil else{
			ManagerCenter.shared.router.changeRoot(to: .login)
			return
		}
		ManagerCenter.shared.router.changeRoot(to: .home)
		self.navigationController?.setNavigationBarHidden(true, animated: true)
	}
	
}
extension SplashController: StoryboardItem {
	static func containerStoryboard() -> ApplicationStoryboard {
		return .main
	}
}
