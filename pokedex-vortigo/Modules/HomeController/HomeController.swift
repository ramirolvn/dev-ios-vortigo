//
//  HomeController.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit
import RxSwift

class HomeController: UIViewController, BaseController {
	@IBOutlet weak var pokemonSearchBar: UISearchBar!
	@IBOutlet weak var pokemonTypeList: UICollectionView!
	@IBOutlet weak var pokemonList: UITableView!
	
	var baseViewModel: BaseViewModelProtocol! {
		didSet {
			viewModel = (baseViewModel as! HomeViewModelProtocol)
		}
	}
	
	var viewModel: HomeViewModelProtocol!
	let disposeBag = DisposeBag()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.makeRequests()
		configView()
	}
	
	private func configView(){
		self.pokemonSearchBar.delegate = self
		configLists()
		configureObservables()
	}
	
	private func configLists(){
		pokemonTypeList.delegate = self
		pokemonTypeList.dataSource = self
		pokemonTypeList.register(UINib(nibName: CollectionViewCellsIdentifiers.pokemonTypeCollection, bundle: nil), forCellWithReuseIdentifier: CollectionViewCellsIdentifiers.pokemonTypeCollection)
		pokemonList.dataSource = self
		pokemonList.register(UINib(nibName: TableViewCellIdentifiers.pokemonCell, bundle: nil),
							 forCellReuseIdentifier: TableViewCellIdentifiers.pokemonCell)
	}
	
	private func configureObservables(){
		viewModel.sectionPokemons.subscribe { [weak self] (sectionPokemons) in
			guard let weakself = self, let _ = sectionPokemons.element else { return }
			weakself.pokemonList.reloadData()
			weakself.stop()
		}.disposed(by: disposeBag)
		
		viewModel.filteredPokemons.subscribe { [weak self] (filteredPokemons) in
			guard let weakself = self, let _ = filteredPokemons.element else { return }
			weakself.pokemonList.reloadData()
			weakself.stop()
		}.disposed(by: disposeBag)
		
		viewModel.pokemonTypesResults.subscribe { [weak self] (pokemonTypesResults) in
			guard let weakself = self, let _ = pokemonTypesResults.element else { return }
			weakself.pokemonTypeList.reloadData()
			weakself.stop()
		}.disposed(by: disposeBag)
		
		viewModel.requestFailure.subscribe { [weak self] (event) in
			guard let weakself = self else { return }
			weakself.stop()
			weakself.showDefaultServerAlertError {[weak self] in
				guard let strongSelf = self else { return }
				strongSelf.makeRequests()
			}
		}.disposed(by: disposeBag)
		
	}
	
	@IBAction func orderByName(_ sender: UIButton) {
		viewModel.orderByName()
	}
	
	private func makeRequests(){
		self.start()
		viewModel.getAllPokemonTypes(disposeBag: self.disposeBag)
		viewModel.getAllPokemon(disposeBag: self.disposeBag)
	}
	
	
}

extension HomeController: UITableViewDataSource{
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.viewModel.rowsInSectionPokemons()
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let pokemonCell = self.pokemonList.dequeueReusableCell(withIdentifier: TableViewCellIdentifiers.pokemonCell) as? PokemonCell, let pokemon = viewModel.getPokemon(at: indexPath.item) else {
			fatalError("Error in mount view")
		}
		pokemonCell.config(pokemon: pokemon)
		return pokemonCell
	}
}
extension HomeController: UICollectionViewDataSource{
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return viewModel.pokemonTypesResults.value?.count ?? 0
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let pokeTypeCell = self.pokemonTypeList.dequeueReusableCell(withReuseIdentifier: CollectionViewCellsIdentifiers.pokemonTypeCollection, for: indexPath) as? PokemonTypeCollectionViewCell, let pokemonType = viewModel.getPokemonType(at: indexPath.item) else{
			fatalError("Error in mount view")
		}
		
		pokeTypeCell.config(pokemonType: pokemonType, isTheSelected: viewModel.isTheFavoriteChosen(pokemonType: pokemonType))
		return pokeTypeCell
	}
	
}

extension HomeController: UICollectionViewDelegate{
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		guard let sectionType = self.viewModel.getPokemonType(at: indexPath.item) else {
			return
		}
		self.viewModel.currentSelectedPokemonType = sectionType
		self.viewModel.filterByCurrentType()
		self.pokemonTypeList.reloadItems(at: self.pokemonTypeList.indexPathsForVisibleItems)
		
	}
}

extension HomeController: StoryboardItem {
	static func containerStoryboard() -> ApplicationStoryboard {
		return .main
	}
}
extension HomeController: UISearchBarDelegate{
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		viewModel.filterBy(name: searchBar.text ?? "")
	}
}
