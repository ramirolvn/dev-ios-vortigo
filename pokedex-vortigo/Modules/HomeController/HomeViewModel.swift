//
//  HomeViewModel.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol HomeViewModelProtocol: BaseViewModelProtocol {
	var pokemonTypesResults: BehaviorRelay<[PokemonType]?> { get }
	var sectionPokemons: BehaviorRelay<[Pokemon]?> { get }
	var requestFailure: PublishRelay<Bool> { get }
	var currentSelectedPokemonType: PokemonType? { get set }
	var filteredPokemons: BehaviorRelay<[Pokemon]?> { get }
	func getPokemon(at index: Int) -> Pokemon?
	func getPokemonType(at index: Int) -> PokemonType?
	func getAllPokemonTypes(disposeBag: DisposeBag)
	func getAllPokemon(disposeBag: DisposeBag)
	func filterByCurrentType()
	func filterBy(name: String)
	func rowsInSectionPokemons() -> Int
	func isTheFavoriteChosen(pokemonType: PokemonType) -> Bool
	func orderByName()
}

class HomeViewModel: HomeViewModelProtocol {
	
	private let provider: NetworkManagerProtocol
	var pokemonTypesResults: BehaviorRelay<[PokemonType]?> = BehaviorRelay(value: nil)
	var allPokemons: BehaviorRelay<[Pokemon]?> = BehaviorRelay(value: nil)
	var sectionPokemons: BehaviorRelay<[Pokemon]?> = BehaviorRelay(value: nil)
	var requestFailure: PublishRelay<Bool> = PublishRelay()
	var currentSelectedPokemonType: PokemonType? = nil
	var filteredPokemons: BehaviorRelay<[Pokemon]?> = BehaviorRelay(value: nil)
	
	init(_ provider: NetworkManagerProtocol, keychain: KeychainProtocol) {
		self.provider = provider
		if let user = keychain.loadUser(){
			self.currentSelectedPokemonType = user.pokemonTypeFavorite
		}
		print("INITIALIZATION -> HomeViewModel")
	}
	
	deinit {
		print("DEINITIALIZATION -> HomeViewModel")
	}
	
	
	func getAllPokemonTypes(disposeBag: DisposeBag){
		provider.getAllPokemonTypes(disposeBag: disposeBag, completion: {(pokemonsTypes,result,error) in
			switch result{
			case .success:
				self.pokemonTypesResults.accept(pokemonsTypes?.results)
			case .failure:
				self.requestFailure.accept(true)
			}
		})
	}
	
	func getAllPokemon(disposeBag: DisposeBag){
		provider.getAllPokemon(disposeBag: disposeBag, completion: {[weak self] (pokemons,result,error) in
			guard let strongSelf = self else { return }
			switch result{
			case .success:
				strongSelf.allPokemons.accept(pokemons)
				strongSelf.filterByCurrentType()
				strongSelf.reArrangeTypes()
			case .failure:
				strongSelf.requestFailure.accept(true)
			}
		})
	}
	
	func getPokemon(at index: Int) -> Pokemon?{
		guard let filtredPokemons = self.filteredPokemons.value, filtredPokemons.count > index else{
			guard let pokemonsSections = self.sectionPokemons.value, pokemonsSections.count > index else{
				return nil
			}
			return pokemonsSections[index]
		}
		return filtredPokemons[index]
	}
	
	func getPokemonType(at index: Int) -> PokemonType?{
		guard let pokemonResults = self.pokemonTypesResults.value, pokemonResults.count > index else {
			return nil
		}
		return pokemonResults[index]
	}
	
	func filterByCurrentType(){
		if let currentTypeSelected  = self.currentSelectedPokemonType{
			let pokemonsAux = self.allPokemons.value?.filter({$0.type.contains(currentTypeSelected.name)})
			self.sectionPokemons.accept(pokemonsAux)
		}
	}
	
	func filterBy(name: String){
		if name.isEmpty == false {
			let filteredPokemons = self.allPokemons.value?.filter({ $0.name.uppercased().contains(name.uppercased()) })
			self.filteredPokemons.accept(filteredPokemons)
		}else{
			self.filteredPokemons.accept(nil)
		}
	}
	
	func rowsInSectionPokemons() -> Int{
		guard let filtredPokemons = self.filteredPokemons.value else {
			guard let typesPokemon = self.sectionPokemons.value else {
				return 0
			}
			return typesPokemon.count
		}
		return filtredPokemons.count
	}
	
	func isTheFavoriteChosen(pokemonType: PokemonType) -> Bool{
		return pokemonType.name == currentSelectedPokemonType?.name && pokemonType.thumbnailImage == currentSelectedPokemonType?.thumbnailImage
	}
	
	func reArrangeTypes(){
		guard let sectionSelected = self.currentSelectedPokemonType, let pokemonTypes = self.pokemonTypesResults.value, let index = pokemonTypes.firstIndex(where: {$0.name.uppercased() == sectionSelected.name.uppercased()}) else {
			reArrangeTypes()
			return
		}
		var pokemonTypesAux = pokemonTypes
		let type = pokemonTypesAux.remove(at: index)
		pokemonTypesAux.insert(type, at: 0)
		self.pokemonTypesResults.accept(pokemonTypesAux)
	}
	
	func orderByName(){
		guard let filtredPokemons = self.filteredPokemons.value else{
			guard let sectionPokemons = self.sectionPokemons.value else{
				return
			}
			let sectionsOrdered = sectionPokemons.sorted{ $0.name.lowercased() < $1.name.lowercased()}
			self.sectionPokemons.accept(sectionsOrdered)
			return
		}
		let filteredOrdered = filtredPokemons.sorted{ $0.name.lowercased() < $1.name.lowercased()}
		self.filteredPokemons.accept(filteredOrdered)
	}
}
