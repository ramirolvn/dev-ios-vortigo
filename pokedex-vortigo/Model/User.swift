//
//  User.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
struct User: Codable {
	let userName: String
	let pokemonTypeFavorite: PokemonType
}
