//
//  Pokemon.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
struct Pokemon: Decodable{
	let abilities: [String]
	let detailPageURL: String
	let weight: Float
	let weakness: [String]
	let number: String
	let height: Float
	let collectibles_slug: String
	let featured: String
	let slug: String
	let name: String
	let thumbnailAltText: String
	let thumbnailImage: String
	let id: Int
	let type: [String]
}
