//
//  DefaultCustomLoader.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import UIKit

class DefaultCustomLoader: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        prepareActivityIndicator()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        prepareActivityIndicator()
    }
    
    // MARK: - View
    private func prepareActivityIndicator() {
        backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		blurEffectView.layer.cornerRadius = 5
        self.addSubview(blurEffectView)
        addActivityIndicator()
    }
    
    private func addActivityIndicator() {
		let indicator = UIActivityIndicatorView(style: .large)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.color = Colors.pinkRed
        indicator.startAnimating()
        self.addSubview(indicator)
        indicator.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        indicator.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    }

}
