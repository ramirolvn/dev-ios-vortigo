//
//  UnderlineDropdownTF.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
import UIKit

class UnderlineDropdownTF: UITextField {
	
	var touchInsideImg: (()->())? = nil
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.configUnderlineDropdownTF()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.configUnderlineDropdownTF()
	}
	
	private func configUnderlineDropdownTF() {
		self.inputView = UIView()
		let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 32, height: 40))
		rightButton.setImage(#imageLiteral(resourceName: "expand-button-white"), for: .normal)
		rightButton.backgroundColor = .clear
		rightButton.imageView?.contentMode = .scaleAspectFill
		rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
		self.rightViewMode = UITextField.ViewMode.always
		
		let border = CALayer()
		let borderWidth = CGFloat(3.0)
		border.borderColor = UIColor.white.cgColor
		border.frame = CGRect(x: 0, y: self.frame.size.height - borderWidth, width: self.frame.size.width, height: self.frame.size.height)
		border.borderWidth = borderWidth
		self.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
		DispatchQueue.main.async {[weak self] in
			guard let strongSelf = self else { return }
			strongSelf.layer.addSublayer(border)
			strongSelf.layer.masksToBounds = true
			strongSelf.borderStyle = .none
			strongSelf.rightView = rightButton
		}
	}
	
	@objc private func rightButtonAction(){
		if let touchInsideImg = self.touchInsideImg{
			touchInsideImg()
		}
	}
	
	
}
