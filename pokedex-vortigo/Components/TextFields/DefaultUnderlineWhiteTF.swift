//
//  DefaultUnderlineWhiteTF.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
import UIKit

class DefaultUnderlineWhiteTF: UITextField {
	
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.configDefaultUnderlineWhiteTF()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.configDefaultUnderlineWhiteTF()
	}
	
	private func configDefaultUnderlineWhiteTF() {
		let border = CALayer()
		let borderWidth = CGFloat(3.0)
		border.borderColor = UIColor.white.cgColor
		border.frame = CGRect(x: 0, y: self.frame.size.height - borderWidth, width: self.frame.size.width, height: self.frame.size.height)
		border.borderWidth = borderWidth
		DispatchQueue.main.async {[weak self] in
			guard let strongSelf = self else { return }
			strongSelf.layer.addSublayer(border)
			strongSelf.layer.masksToBounds = true
			strongSelf.borderStyle = .none
		}
	}
	
}
