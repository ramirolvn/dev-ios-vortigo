//
//  RedMainButton.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 23/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation
import UIKit

class RedMainButton: UIButton {
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.configRedMainButton()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.configRedMainButton()
	}
	
	private func configRedMainButton() {
		self.layer.cornerRadius = 8
		self.titleLabel?.font = UIFont(name: "Noteworthy-Bold", size: 17.0)
		self.setTitleColor(.white, for: .normal)
		self.tintColor = .white
		self.enable()
	}
	
	func disable() {
		self.isEnabled = false
		self.backgroundColor = .gray
	}
	
	func enable() {
		self.isEnabled = true
		self.backgroundColor = Colors.pinkRed
		
	}
	
}
