//
//  Enviroment.swift
//  pokedex-vortigo
//
//  Created by Ramiro Lima Vale Neto on 20/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import Foundation

enum Environment {
    case dev
    case prod
    
    var baseUrl: String {
        switch self {
        case .dev:
            return "https://vortigo.blob.core.windows.net"
        case .prod:
            return "https://vortigoprod.blob.core.windows.net"
        }
    }
}
