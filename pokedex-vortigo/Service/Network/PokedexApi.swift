import Foundation
import Moya

enum PokedexApi {
	case allPokemonTypes
	case allPokemons
}

extension PokedexApi: TargetType {
	var baseURL: URL {
		return URL(string: AppInitialConfig.shared.ENVIRONMENT!.baseUrl)!
	}
	
	var path: String {
		switch self {
		case .allPokemonTypes:
			return "/files/pokemon/data/types.json"
		case .allPokemons:
			return "/files/pokemon/data/pokemons.json"
		}
	}
	
	var method: Moya.Method {
		switch self {
		case .allPokemonTypes,
			 .allPokemons:
			return .get
		}
	}
	
	var sampleData: Data {
		return Data()
	}
	
	var task: Task {
		switch self {
		case .allPokemonTypes,
			 .allPokemons:
			return .requestPlain
		}
	}
	
	var headers: [String : String]? {
		switch self {
		case .allPokemonTypes,
			 .allPokemons:
			return ["Content-type": "application/json"]
		}
	}
}
