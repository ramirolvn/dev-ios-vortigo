import Foundation
import Moya
import RxSwift
import Alamofire

class NetworkManager: NetworkManagerProtocol {
	var provider: MoyaProvider<PokedexApi> = MoyaProvider<PokedexApi>()
	
	
	// MARK: - GetAllPokemonTypes
	func getAllPokemonTypes(disposeBag: DisposeBag, completion: @escaping ((PokemonTypesResults?, RequestStatus, Error?) -> Void)) {
		provider.rx.request(.allPokemonTypes).map(PokemonTypesResults.self)
			.subscribe { (event) in
				switch event {
				case .success(let pokemonTypesResults):
					completion(pokemonTypesResults, .success, nil)
				case .error(let error):
					completion(nil, .failure, error)
				}
		}.disposed(by: disposeBag)
	}
	
	//MARK: - GetAllPokemons
	func getAllPokemon(disposeBag: DisposeBag, completion: @escaping (([Pokemon]?, RequestStatus, Error?) -> Void)) {
		provider.rx.request(.allPokemons).map([Pokemon].self)
			.subscribe { (event) in
				switch event {
				case .success(let pokemons):
					completion(pokemons, .success, nil)
				case .error(let error):
					completion(nil, .failure, error)
				}
		}.disposed(by: disposeBag)
	}
}
