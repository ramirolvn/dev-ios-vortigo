import Foundation
import Moya
import RxSwift

protocol NetworkManagerProtocol {
	var provider: MoyaProvider<PokedexApi> { get }
	
	func getAllPokemonTypes(
		disposeBag: DisposeBag,
		completion: @escaping ((PokemonTypesResults?,RequestStatus, Error?) -> Void)
	)
	
	func getAllPokemon(
		disposeBag: DisposeBag,
		completion: @escaping (([Pokemon]?,RequestStatus, Error?) -> Void)
	)
}
