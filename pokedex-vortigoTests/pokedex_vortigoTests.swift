//
//  pokedex_vortigoTests.swift
//  pokedex-vortigoTests
//
//  Created by Ramiro Lima Vale Neto on 20/05/20.
//  Copyright © 2020 Ramiro Lima Vale Neto. All rights reserved.
//

import XCTest
import RxSwift
@testable import pokedex_vortigo

class pokedex_vortigoTests: XCTestCase {
	var sutHome: HomeViewModel!
	var pokemonsMock: [Pokemon]!
	var pokemonTypeMock: [PokemonType]!
	
	override func setUp() {
		super.setUp()
		sutHome = HomeViewModel(NetworkManager(), keychain: KeyChain())
		AppInitialConfig.shared.ENVIRONMENT = .dev
		self.pokemonsMock = [Pokemon(abilities: [], detailPageURL: "", weight: 10, weakness: [], number: "", height: 10, collectibles_slug: "", featured: "", slug: "", name: "Pikachu", thumbnailAltText: "", thumbnailImage: "", id: 1, type: []), Pokemon(abilities: [], detailPageURL: "", weight: 10, weakness: [], number: "", height: 10, collectibles_slug: "", featured: "", slug: "", name: "Abra", thumbnailAltText: "", thumbnailImage: "", id: 1, type: []), Pokemon(abilities: [], detailPageURL: "", weight: 10, weakness: [], number: "", height: 10, collectibles_slug: "", featured: "", slug: "", name: "Poliwrath", thumbnailAltText: "", thumbnailImage: "", id: 1, type: [])]
		self.pokemonTypeMock = [PokemonType(thumbnailImage: "https://vortigo.blob.core.windows.net/files/pokemon/assets/fighting.png", name: "fighting"), PokemonType(thumbnailImage: "https://vortigo.blob.core.windows.net/files/pokemon/assets/rock.png", name: "rock")]
	}
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
	
	func testFavoriteCategory(){
		sutHome.currentSelectedPokemonType = PokemonType(thumbnailImage: "https://vortigo.blob.core.windows.net/files/pokemon/assets/fighting.png", name: "fighting")
		XCTAssert(sutHome.isTheFavoriteChosen(pokemonType: pokemonTypeMock[0]))
	}
	
	func testOrderByName(){
		sutHome.sectionPokemons.accept(pokemonsMock)
		sutHome.orderByName()
		XCTAssert(sutHome.sectionPokemons.value?.first?.name == pokemonsMock[1].name)
	}
	
	func testFilter(){
		sutHome.allPokemons.accept(pokemonsMock)
		sutHome.filterBy(name: "Abra")
		XCTAssert(sutHome.filteredPokemons.value?.count == 1)
		XCTAssert(sutHome.filteredPokemons.value?.first?.name == "Abra")
	}
	
	func testRowsCount(){
		sutHome.sectionPokemons.accept(pokemonsMock)
		XCTAssert(sutHome.rowsInSectionPokemons() == 3)
	}

}
